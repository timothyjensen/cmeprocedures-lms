# README #

This plugin allows customizes the Learndash LMS for the CME Procedures website. Basic instructions for shortcode usage are included in WP Admin, `wp-admin/admin.php?page=learndash-lms-certificate_shortcodes`.

## Credits
Built by [Tim Jensen](https://github.com/timothyjensen).
