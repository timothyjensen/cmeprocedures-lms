<?php
/**
 * Learndash Quiz Meta
 *
 * @package     CMEProcedures\LMS
 * @author      Tim Jensen <tim@timjensen.us>
 * @license     GNU General Public License 2.0+
 * @since       0.1.0
 */

namespace CMEProcedures\LMS;

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

/**
 * Class Learndash_Quiz_Meta
 *
 * @package CMEProcedures\LMS
 */
class Learndash_Quiz_Meta {

	/**
	 * Quiz ID
	 *
	 * @var int
	 */
	protected $user_id;

	/**
	 * Quiz ID
	 *
	 * @var int
	 */
	protected $quiz_id;

	/**
	 * Quiz custom field data.
	 *
	 * @var mixed
	 */
	protected $quiz_form_data = null;

	/**
	 * Error logger.
	 *
	 * @var \Monolog\Logger
	 */
	protected $error_logger;

	/**
	 * Learndash_Quiz_Meta constructor.
	 *
	 * @param int $user_id
	 * @param int $quiz_id
	 */
	public function __construct( int $user_id, int $quiz_id ) {
		$this->user_id = $user_id;
		$this->quiz_id = $quiz_id;

		$this->error_logger = new Logger( __CLASS__ );

		$error_log_path = CME_PROCEDURES_LMS_DIR . '/logs/error.log';
		$this->error_logger->pushHandler( new StreamHandler( $error_log_path, Logger::DEBUG ) );
	}

	/**
	 * Retrieves the form data.
	 *
	 * @return array
	 */
	public function get_quiz_form_data() {
		if ( is_null( $this->quiz_form_data ) ) {
			$this->quiz_form_data = $this->query_quiz_form_data();
		}

		return $this->quiz_form_data;
	}

	/**
	 * Queries the quiz form data.
	 *
	 * @return array
	 */
	protected function query_quiz_form_data(): array {
		global $wpdb;

		$sql = "
			SELECT *
			FROM {$wpdb->prefix}wp_pro_quiz_statistic_ref AS wpqsf
			WHERE wpqsf.user_id = %d
			AND wpqsf.quiz_id = %d
			ORDER BY wpqsf.statistic_ref_id DESC
		";

		$results = $wpdb->get_row( $wpdb->prepare( $sql, $this->user_id, $this->quiz_id ) );

		if ( empty( $results->form_data ) ) {
			$this->error_logger->error( 'No form data found', [ $wpdb->last_query ] );
		}

		$form_data = json_decode( $results->form_data, true );

		if ( is_null( $form_data ) ) {
			$this->error_logger->error( 'Form data is not in JSON format', [ $form_data ] );
		}

		return array_values( (array) $form_data );
	}
}
