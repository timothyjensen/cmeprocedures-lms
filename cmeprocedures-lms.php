<?php
/**
 * Plugin Name: CME Procedures LMS
 * Plugin URI: https://bitbucket.org/timothyjensen/cmeprocedures-lms
 * Description: Customizes the CME Procedures LMS.
 *
 * Version: 1.0.2
 *
 * Author: Tim Jensen
 * Author URI: https://www.timjensen.us
 *
 * This program is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License version 2, as published by the
 * Free Software Foundation.  You may NOT assume that you can use any other
 * version of the GPL.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Text Domain: cmeprocedures-lms
 *
 * @package CMEProcedures\LMS
 *
 * PHP Version 7.0+
 */

namespace CMEProcedures\LMS;

if ( ! defined( 'ABSPATH' ) ) {
	die;
}

define( 'CME_PROCEDURES_LMS_DIR', __DIR__ );
define( 'CME_PROCEDURES_LMS_CONFIG_DIR', __DIR__ . '/config' );
define( 'CME_PROCEDURES_LMS_VIEWS_DIR', __DIR__ . '/views' );
define( 'CME_PROCEDURES_LMS_FILE', __FILE__ );
define( 'CME_PROCEDURES_LMS_URL', plugins_url( null, __FILE__ ) );

add_action( 'plugins_loaded', __NAMESPACE__ . '\\load_frontend_files' );
/**
 * Loads plugin files.
 */
function load_frontend_files() {
	// Only run if Learndash if active.
	if ( ! class_exists( 'SFWD_CPT' ) ) {
		return;
	}

	$files = [
		'vendor/autoload.php',
		'includes/helpers.php',
		'includes/shortcode.php',
	];

	array_walk( $files, function( $file ) {
		require_once CME_PROCEDURES_LMS_DIR . '/' . $file;
	} );
}

add_action( 'admin_init', __NAMESPACE__ . '\\load_admin_files' );
/**
 * Loads admin files.
 */
function load_admin_files() {
	// Only run if Learndash if active.
	if ( ! class_exists( 'SFWD_CPT' ) ) {
		return;
	}

	$files = [
		'includes/admin.php',
	];

	array_walk( $files, function( $file ) {
		require_once CME_PROCEDURES_LMS_DIR . '/' . $file;
	} );
}
