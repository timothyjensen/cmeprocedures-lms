<?php
/**
 * Shortcodes configuration array.
 *
 * @package        CMEProcedures\LMS
 * @author         Tim Jensen <tim@timjensen.us>
 * @license        GNU General Public License 2.0+
 * @link           https://www.timjensen.us
 * @since          0.1.0
 */

namespace CMEProcedures\LMS;

/**
 * Shortcode configuration.
 *
 * @see https://codex.wordpress.org/Function_Reference/add_shortcode
 */
return [
	[
		'tag'  => 'quiz-form-data',
		'args' => [
			'field'  => '1', // To be more intuitive we are not zero-indexing the fields.
			'format' => 'F j, Y',
		],
		'view' => CME_PROCEDURES_LMS_VIEWS_DIR . '/shortcode/quiz-form-data.php',
	],
];
