<?php
/**
 * Extends Learndash Admin pages
 *
 * @package     CMEProcedures\LMS
 * @author      Tim Jensen <tim@timjensen.us>
 * @license     GNU General Public License 2.0+
 * @since       0.1.0
 */

namespace CMEProcedures\LMS;

add_action( 'sfwd-certificates_page_learndash-lms-certificate_shortcodes', __NAMESPACE__ . '\\render_shortcode_instructions', 15 );
/**
 * Renders the shortcode instructions.
 */
function render_shortcode_instructions() {
	include CME_PROCEDURES_LMS_VIEWS_DIR . '/admin/admin.php';
}
