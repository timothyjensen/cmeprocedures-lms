<?php
/**
 * Helper functions.
 *
 * @package     CMEProcedures\LMS
 * @author      Tim Jensen <tim@timjensen.us>
 * @license     GNU General Public License 2.0+
 * @since       0.1.0
 */

namespace CMEProcedures\LMS;

/**
 * Queries the quiz form data and caches it for subsequent requests.
 *
 * @param int $user_id User ID
 * @param int $quiz_id Quiz ID
 * @return mixed
 */
function get_cached_quiz_form_data( $user_id, $quiz_id ) {
	static $quiz_meta;

	if ( ! isset( $quiz_meta[ $user_id ][ $quiz_id ] ) ) {
		$form_data = new Learndash_Quiz_Meta( (int) $user_id, (int) $quiz_id );
		$form_data = $form_data->get_quiz_form_data();

		$quiz_meta[ $user_id ][ $quiz_id ] = $form_data;
	}

	return $quiz_meta[ $user_id ][ $quiz_id ];
}
