<?php
/**
 * Shortcodes module.
 *
 * @package     CMEProcedures\LMS
 * @since       0.1.0
 * @author      Tim Jensen
 * @link        https://www.cmeprocedures.com/
 * @license     GNU General Public License 2.0+
 */

namespace CMEProcedures\LMS;

add_action( 'init', __NAMESPACE__ . '\\register_shortcodes' );
/**
 * Register shortcodes.
 *
 * @return void
 */
function register_shortcodes() {

	$config = include CME_PROCEDURES_LMS_CONFIG_DIR . '/shortcode.php';

	foreach ( (array) $config as $shortcode_config ) {
		( new Shortcode( $shortcode_config ) )->init();
	}
}
