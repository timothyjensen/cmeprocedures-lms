<div class="wrap">
	<div class="sfwd_options_wrapper sfwd_settings_left">
		<div class="postbox " id="cme-procedures-certificates-shortcode-metabox">
			<div class="inside" style="margin: 11px 0; padding: 0 12px 12px;">
				<h3>Additional Shortcode Options</h3>
				<p class="ld-shortcode-header">[quiz-form-data]</p>
				<p>This shortcode displays the data that users enter at the end of quizzes:</p>
				<ol class="cert_shortcode_parm_list">
					<li><code>field</code> <b>Required</b>. Use "1" to show the data saved from the first form field, "2" to show the data saved from the second form field, and so on.</li>
					<li><code>format</code> Specify the date format to use. Defaults to the format of <?php echo esc_html( date( 'F j, Y' ) ); ?>. See the example below.</li>
				</ol>
				<p>Example: <b>[quiz-form-data field="1"][quiz-form-data field="2" format="F-j-Y"]</b> shows the data from the first form field and the second form field using the time format "F-j-Y".</p>
		</div>
	</div>
</div>
