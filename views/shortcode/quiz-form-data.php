<?php
/**
 * Shortcode view file.
 *
 * @var $atts
 */

namespace CMEProcedures\LMS;

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

$error_logger = new Logger( __FILE__ );
$error_logger->pushHandler( new StreamHandler(
	CME_PROCEDURES_LMS_DIR . '/logs/error.log',
	Logger::DEBUG
) );

$field_num = $atts['field'] ? $atts['field'] - 1 : null;

if ( is_null( $field_num ) ) {
	$error_logger->error( 'Missing required field number for shortcode %s' );

	return;
}

$date_format = $atts['format'];

$quiz_post_id = filter_input( INPUT_GET, 'quiz', FILTER_VALIDATE_INT );
$course_id    = filter_input( INPUT_GET, 'course_id', FILTER_VALIDATE_INT );

// If no quiz ID is in the request then attempt to get the quiz ID using the course ID.
if ( ! isset( $quiz_post_id ) && isset( $course_id ) ) {
	$quizzes = learndash_get_course_quiz_list( $course_id );

	// Get the first quiz.
	$quiz_post_id = reset( $quizzes )['post']->ID ?? null;
}

if ( ! $quiz_post_id ) {
	$error_logger->error( sprintf( 'Could not find quiz post ID for course %s', $course_id ) );

	return;
}

$quiz = filter_var(
	get_post_meta( $quiz_post_id, 'quiz_pro_id', true ),
	FILTER_VALIDATE_INT
);

if ( ! $quiz ) {
	$error_logger->error( sprintf( 'Could not find quiz ID for course %s and quiz %s', $course_id, $quiz_post_id ) );

	return;
}

$user_id = get_current_user_id();

// Allow admins and group_leaders to view certificate from other users.
if ( get_post_type() === 'sfwd-certificates' ) {
	if (
		( learndash_is_admin_user() || learndash_is_group_leader_user() )
		&& ! empty( $_GET['user'] )
	) {
		$user_id = intval( $_GET['user'] );
	}
}

if ( empty( $user_id ) ) {
	return;
}

$quiz_meta = get_cached_quiz_form_data( $user_id, $quiz );

$form_value = $quiz_meta[ $field_num ] ?? '';

// Convert dates to the specified date format.
$is_date = preg_match( '/[0-9]{1,2}-[0-9]{1,2}-[0-9]{4}/', $form_value );
if ( $is_date ) {
	$form_value = date( $date_format, strtotime( $form_value ) );
}

echo esc_html( $form_value );
